<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";
$today= "";
$diff="";
$facultate="";
$gen="";
$ok=0;


if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}
if(isset($_POST['gen'])){
	$gen = $_POST['gen'];
}
if(isset($_POST['ok'])){
	$ok = $_POST['ok'];
}

if(empty($firstname)||empty($lastname)||empty($phone)||empty($email)||empty($cnp)||empty($facebook)||empty($birth)||empty($department)||empty($question)||empty($captcha_generated)||empty($check)){

	$error = 1;
	$error_text = "One or more fields are empty";
}

if(strlen($firstname) < 3 || strlen($firstname) > 20 ||strlen($lastname) < 3||strlen($lastname) > 20 || strlen($question) < 15){

	$error = 1;
	$error_text = "The name is too big or too small, or the answer is too small";

}

if(!is_string($firstname) || !is_string($lastname)){

	$error = 1;
	$error_text = "the does not contain only letters";
}

if(!is_numeric($phone)||strlen($phone) != 10){

	$error=1;
	$error_text= "The phone number is wrong";
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $error_text="the email is incorrect";
      $error=1;
     

 }
if(strlen($facultate)<3 || strlen($facultate)>30 )
{
	$error_text="Your college name is either too short, too long or it does contains digits";
	$error=1;

}
 if(strlen($cnp)!=13 || ($cnp[0]!=1 && $cnp[0]!=2 && $cnp[0]!=3 && $cnp[0]!= 4 && $cnp[0]!= 5 && $cnp[0]!=6)){

 	$error=1;
 	$error_text="The cnp is incorrect ";
 	
 
 }

 if($captcha_generated!=$captcha_inserted){

 	$error=1;
 	$error_text="the captcha is worng";


 }


 if (filter_var($facebook, FILTER_VALIDATE_URL)){
 	$error=1;
 	$error_text="The link from Faebook is incorrect";

 }
$today = date("Y-m-d");
$diff = date_diff(date_create($birth), date_create($today));

if($diff->format('%y')<18|| $diff->format('/y')>100){

	$error=1;
	$error_text="You are too young to be here";
}


if($cnp[1]!=$birth[2]||$cnp[2]!=$birth[3]||$cnp[3]!=$birth[5]||$cnp[4]!=$birth[6]||$cnp[5]!=$birth[8]||$cnp[6]!=$birth[9])
{

	$error_text="cnp and birth does not match";
	$error=1;
	echo $birth[4];
	echo $cnp;
}

if ($cnp[0]=='1'||$cnp[0]=='3'||$cnp[0]=='5') {
	$gen="M";
}
if ($cnp[0]=='2'||$cnp[0]=='4'||$cnp[0]=='6') {
	$gen="F";
}



if(empty($firstname)||empty($lastname)||empty($phone)||empty($email)||empty($cnp)||empty($facebook)||empty($birth)||empty($department)||empty($question)||empty($captcha_generated)||empty($check)){

	$error = 1;
	$error_text = "One or more fields are empty";
}

if(strlen($firstname) < 3 || strlen($firstname) > 20 ||strlen($lastname) < 3||strlen($lastname) > 20 || strlen($question) < 15){

	$error = 1;
	$error_text = "The name is too big or too small, or the answer is too small";

}

if(!is_string($firstname) || !is_string($lastname)){

	$error = 1;
	$error_text = "the does not contain only letters";
}

if(!is_numeric($phone)||strlen($phone) != 10){

	$error=1;
	$error_text= "The phone number is wrong";
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $error_text="the email is incorrect";
      $error=1;
     

 }

 if(strlen($cnp)!=13 || ($cnp[0]!=1 && $cnp[0]!=2 && $cnp[0]!=3 && $cnp[0]!= 4 && $cnp[0]!= 5 && $cnp[0]!=6)){

 	$error=1;
 	$error_text="The cnp is incorrect ";
 	echo $cnp[0];
 
 }

 if($captcha_generated!=$captcha_inserted){

 	$error=1;
 	$error_text="the captcha is worng";


 }


 if (filter_var($facebook, FILTER_VALIDATE_URL)){
 	$error=1;
 	$error_text="The link from Faebook is incorrect";

 }

$today = date("Y-m-d");
$diff = date_diff(date_create($birth), date_create($today));

if($diff->format('%y')<18|| $diff->format('/y')>100){

	$error=1;
	$error_text="You are too young to be here";
}




try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
	if($error == 0){
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate,gen) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question),:facultate,:gen");



$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':gen',$gen);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";

}
} else {

	echo $error_text;

}




